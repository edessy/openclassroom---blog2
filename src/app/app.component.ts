import { Component } from '@angular/core';
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor() {
    const config = {
      apiKey: "AIzaSyCMmBJ2S6XPBIV79gDH_1nj9XJT4NMsmlw",
      authDomain: "postappocr.firebaseapp.com",
      databaseURL: "https://postappocr.firebaseio.com",
      projectId: "postappocr",
      storageBucket: "postappocr.appspot.com",
      messagingSenderId: "383891209767",
      appId: "1:383891209767:web:404be007cd41a856d39802"
    };
    firebase.initializeApp(config);
  }
}
