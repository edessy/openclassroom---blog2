import { Component, OnInit, Input } from '@angular/core';
import {Post} from "../models/post.model";
import {PostService} from "../services/post.service";

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post : Post;

  constructor(private postsService : PostService) { }

  ngOnInit() {
  }

  updateLoveIts(loveIt) {
    this.post.loveIts += loveIt;
    this.postsService.emitPosts();
    this.postsService.savePosts();
  }

  onDeletePost(post: Post) {
    this.postsService.removePost(post);
  }

}
